<?php
if (!defined('THINK_PATH')) {
    exit();
}
define('ADMIN_PERMISSION_CHECK_IGNORE', true);

$err = error_reporting(E_ALL & (~E_NOTICE));
set_error_handler(function () {

});

$db = \Think\Db::getInstance();
$db_prefix = C('DB_PREFIX');
$db_type = C('DB_TYPE');
$tables = $db->getTables();

$msgs = array();


/////////////////////////////// app custom upgrade ///////////////////////////////

if (file_exists($file = './app/Common/Upgrade/custom.php')) {
    include $file;
}

/////////////////////////////// db repair or upgrade ///////////////////////////////

$table_name = $db_prefix . "cms_tag_pool";
if (!in_array($table_name, $tables)) {
    $db->execute("
						CREATE TABLE ${table_name} (
						  id INT unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
						  addtime INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
						  updatetime INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
						  cat VARCHAR(50) NOT NULL DEFAULT '' COMMENT '分类',
						  name VARCHAR(100) DEFAULT NULL DEFAULT '' COMMENT '名称',
						  PRIMARY KEY (id),
						  KEY addtime (addtime),
						  KEY updatetime (updatetime),
						  KEY cat (cat),
						  KEY name (name)
						) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT '标签池';");
    $msgs [] = '创建cms_tag_pool数据表';
}

$table_name = $db_prefix . "data_files";
if (in_array($table_name, $tables)) {
    $result = $db->query("DESC $table_name");
    if (is_array($result)) {
        $fields = array();
        foreach ($result as &$r) {
            $fields[] = $r['field'];
        }
        if (!in_array('filesize', $fields)) {
            $db->execute("ALTER TABLE $table_name ADD filesize INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小' AFTER uptime;");
            $msgs[] = 'data_files添加filesize字段';
        }
        if (!in_array('filename', $fields)) {
            $db->execute("ALTER TABLE $table_name ADD filename VARCHAR(100) NOT NULL DEFAULT '' COMMENT '文件名称' AFTER path;");
            $msgs[] = 'data_files添加filename字段';
        }
    }
}
node_init();
$msgs[] = '重新计算后台管理权限节点';

/////////////////////////////// init mod & cms ///////////////////////////////
foreach (get_controllers('Admin', 'mod') as $controller) {
    A('Admin/Mod' . $controller ['name'])->build($yummy = true);
}
foreach (get_controllers('Admin', 'cms') as $controller) {
    A('Admin/Cms' . $controller ['name'])->build($yummy = true);
}

/////////////////////////////// clear cache ///////////////////////////////
tpx_rm_dir('./_RUN/', false);
if (!file_exists('./_RUN/')) {
    @mkdir('./_RUN/');
}
@file_put_contents('./_RUN/index.html', 'Access Forbidden');

echo '<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>升级提示</title>
</head>
<body>
<div style="margin:0 auto;width:600px;background:#EEE;line-height:2em;font-size:12px;padding:1em;">
<div style="border-bottom:1px solid #CCC;font-size:14px;text-align: center;color:red;">升级提示</div>
<ul>
';
if (!empty($msgs)) {
    foreach ($msgs as $msg) {
        echo "<li>" . $msg . "</li>";
    }
    \Think\Log::write("Upgrade to V" . APP_VERSION . ', ' . join(",", $msgs) . "\r\n", \Think\Log::INFO);
}
echo '
<li>升级版本到 V' . APP_VERSION . '</li>
</ul>
</div>
</body>
</html>';
exit();



